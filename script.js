const tomato = document.querySelector('.tomato');
const orange = document.querySelector('.orange');
const blue = document.querySelector('.dodgerBlue');
const slateBlue = document.querySelector('.slateBlue');
const violet = document.querySelector('.violet');

const colorGainer = document.getElementById('colorGainer');

const getBGColor = (selectedElement) => {
    return window.getComputedStyle(selectedElement).backgroundColor;
}

const magicColorChanger = (element, color) => {
    return element.addEventListener('mouseenter', () => {
        colorGainer.style.background = color
    })
}

magicColorChanger(tomato, getBGColor(tomato));
magicColorChanger(orange, getBGColor(orange));
magicColorChanger(blue, getBGColor(blue));
magicColorChanger(slateBlue, getBGColor(slateBlue));
magicColorChanger(violet, getBGColor(violet));